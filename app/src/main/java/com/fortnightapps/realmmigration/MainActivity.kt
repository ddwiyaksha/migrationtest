package com.fortnightapps.realmmigration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.realm.Realm
import io.realm.RealmConfiguration

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Realm.init(this)

        val config = RealmConfiguration.Builder()
            .schemaVersion(2) // Must be bumped when the schema changes
            .migration(MyMigration()) // Migration to run instead of throwing an exception
            .build()
        Realm.setDefaultConfiguration(config)
        Realm.getDefaultInstance()
    }


}
