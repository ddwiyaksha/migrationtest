package com.fortnightapps.realmmigration

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Creature(
    @PrimaryKey
    var id: String = "",
    var name: String = ""
): RealmObject()