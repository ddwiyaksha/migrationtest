package com.fortnightapps.realmmigration

import io.realm.*

open class MyMigration : RealmMigration {


    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        for(currentVersion in (oldVersion+1)..newVersion) {
            runMigration(realm, currentVersion)
        }
    }

    open fun runMigration(realm: DynamicRealm, version: Long) {
        when(version) {
            0L -> firstMigration(realm)
            1L -> secondMigration(realm)
            2L -> thirdMigration(realm)
            3L -> fourthMigration(realm)
        }
    }

    private fun fourthMigration(realm: DynamicRealm) {
        val creatureSchema = realm.schema.get("Creature")
//
        if(creatureSchema?.isPrimaryKey("id") != true) {
            deleteDuplicateData(realm)
            creatureSchema?.addPrimaryKey("id")
        }
    }

    private fun deleteDuplicateData(realm: DynamicRealm) {
        val idMap = mutableMapOf<String, Boolean>()
        val data = realm.where(Creature::class.java.simpleName)
            .findAll()
            .toList()
            .reversed()
        val duplicateData = data.filter {
            if (idMap[it.getString("id")] != true) {
                idMap[it.getString("id")] = true
                false
            } else {
                true
            }
        }
        duplicateData.forEach {
            it.deleteFromRealm()
        }
    }

    private fun thirdMigration(realm: DynamicRealm) {
        realm.schema.get("Creature")
            ?.addPrimaryKey("id")
    }

    private fun secondMigration(realm: DynamicRealm) {
        realm.schema.get("Creature")
            ?.addField("id", String::class.java, FieldAttribute.REQUIRED)
    }

    private fun firstMigration(realm: DynamicRealm) {
        realm.schema.create("Creature")
            .addField("name", String::class.java, FieldAttribute.REQUIRED)
    }

}
