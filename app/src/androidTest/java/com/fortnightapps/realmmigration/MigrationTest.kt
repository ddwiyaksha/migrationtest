package com.fortnightapps.realmmigration

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import io.realm.DynamicRealm
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.exceptions.RealmMigrationNeededException
import io.realm.kotlin.where
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MigrationTest {
    @Rule
    @JvmField
    var configFactory = TestRealmConfigurationFactory()

    private var context: Context? = null


    @Before
    @Throws(Exception::class)
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test(expected = RealmMigrationNeededException::class)
    @Throws(Exception::class)
    fun migrate_migrationNeededIsThrown() {
        val REALM_NAME = "realm_migration_default.realm"
        val realmConfig = RealmConfiguration.Builder()
            .name(REALM_NAME)
            .schemaVersion(1) // Original realm version.
            .migration(MyMigration())
            .build()// Get a configuration instance.
        var realm: Realm? = null

        try {

            configFactory.copyRealmFromAssets(
                context,
                REALM_NAME,
                realmConfig
            ) // Copy the stored version 1 realm file from assets to a NEW location.
            realm = Realm.getInstance(realmConfig)

        } finally {
            realm?.close()

            Realm.deleteRealm(realmConfig)

        }
    }

    @Test
    @Throws(Exception::class)
    fun migrate_success() {
        val REALM_NAME = "realm_migration_default.realm"
        val realmConfig = RealmConfiguration.Builder()
            .name(REALM_NAME)
            .schemaVersion(5)
            .migration(MyMigration())
            .build()// Get a configuration instance.
        var realm: Realm? = null

        try {
            configFactory.copyRealmFromAssets(
                context,
                REALM_NAME,
                realmConfig
            )
            realm = Realm.getInstance(realmConfig)

        } finally {
            realm?.close()

            Realm.deleteRealm(realmConfig)
        }
    }

    @Test
    @Throws(Exception::class)
    fun migrate_with_duplicate() {
        val REALM_NAME = "realm_migration_default.realm"
        val realmConfig = RealmConfiguration.Builder()
            .name(REALM_NAME)
            .schemaVersion(5)
            .migration(MigrationWithDuplicateData())
            .build()// Get a configuration instance.
        var realm: Realm? = null

        try {
            configFactory.copyRealmFromAssets(
                context,
                REALM_NAME,
                realmConfig
            )
            realm = Realm.getInstance(realmConfig)

            val result = realm?.where<Creature>()
                ?.findAll()

            Assert.assertEquals(2, result?.size)
            Assert.assertEquals("1", result?.get(0)?.id)
            Assert.assertEquals("tiga", result?.get(0)?.name)
            Assert.assertEquals("2", result?.get(1)?.id)
            Assert.assertEquals("dua", result?.get(1)?.name)
        } finally {

            realm?.close()

            Realm.deleteRealm(realmConfig)
        }
    }
}


class MigrationWithDuplicateData : MyMigration() {

    override fun runMigration(realm: DynamicRealm, version: Long) {
        when(version) {
            1L -> {
                super.runMigration(realm, version)
                val creatures = listOf(
                    Creature("1", "satu"),
                    Creature("1", "dua"),
                    Creature("1", "tiga"),
                    Creature("2", "dua")
                )
                creatures.forEach { creature ->
                    val creatureObject = realm.createObject("Creature")
                    creatureObject["id"] = creature.id
                    creatureObject["name"] = creature.name
                }
            }
            2L -> try {
                super.runMigration(realm, version)
            } catch (e: IllegalArgumentException) {

            }
            else -> super.runMigration(realm, version)
        }
    }

}